import socket
import thread

import HTTPClient

def main():
    serverSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    serverSocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    serverSocket.bind(('', 80))
    serverSocket.listen(1)

    try:
        while True:
            channel, details = serverSocket.accept()
            thread.start_new_thread(HTTPClient.HTTPClient, (channel, details))
    except KeyboardInterrupt:
        serverSocket.close()
        exit()

if __name__ == '__main__':
    main()
