'Set the Document Root'
documentRoot = "/var/www/"

'Set the collection of MIME Types'
MIMEList = { "html" :   "text/html",
             "css"  :   "text/css",
             "xml"  :   "text/xml",
             "gif"  :   "image/gif",
             "jpg"  :   "image/jpeg",
             "js"   :   "application/x-javascript",
             "png"  :   "image/png",
             "ico"  :   "image/x-icon",
             ""     :   "text/plain" }
