import os
import shlex, subprocess
import time

from HTTPConfig import *

def in_directory(file, directory):
    #make both absolute    
    directory = os.path.realpath(directory)
    file = os.path.realpath(file)

    #return true, if the common prefix of both is equal to directory
    #e.g. /a/b/c/d.rst and directory is /a/b, the common prefix is /a/b
    return os.path.commonprefix([file, directory]) == directory

class HTTPClient:
    def __init__(self, channel, details):
        try:
            self.channel = channel
            self.details = details
            self.output = ""
            self.connection_handler()
            exit()
        except KeyboardInterrupt:
            channel.close()
            exit()

    def connection_handler(self):
        'Runs through the steps of handling an HTTP request'
        print 'We have opened a connection with', self.details
        self.output = ( 'HTTP/1.1 200 OK\r\nContent-Type: text/html\r\n\r\n'
                        '<html>\r\n<body>\r\n'
                        '<form action="/submit.py" method="POST">\r\n'
                        '   First name: <input type="text" name="fname"><br>\r\n'
                        '   Last name: <input type="text" name="lname"><br>\r\n'
                        '   <input type="submit" value="Submit">\r\n'
                        '</form>\r\n'
                        '</body>\r\n</html>\r\n' )
        self.createHTTPParams()
        self.parseRequestedFile()
        self.handleRequest()
        self.channel.send ( self.output )
        self.channel.close()

    def createHTTPParams(self):
        'Creates a list of HTTP Parameters sent by the client'
        methodReceived = False
        parameters = ""
        parameterslist = []
        self.HTTPParams = {}
        self.GETParams = {}
        self.POSTParams = {}
        self.content = ""

        while True:
            if (parameters[-4:] == "\r\n\r\n"):
                break
            else:
                parameters += self.channel.recv(1)

        parameterslist = parameters.split("\r\n")

        #change by ArchM - build parameter dictionary 
        #since we are using a HTTP we can expect a particular format for requests coming into the server
        #see rfc2616 section 5 for the request header format, this is based upon that:
        #here x is unused, just there as a placeholder
        #we want our Server to only respond to GET, HEAD, or POST requests, so check that here - 
        #ideally this would be a global variable that the user can change on the command line
        methodsDesired = ["GET", "HEAD", "POST"]
        self.HTTPParams["METHOD"], x,  parameterslist[0] = parameterslist[0].partition(" ") #set method parameter
        if self.HTTPParams["METHOD"] not in methodsDesired:
            httpError(400)
        
        self.HTTPParams["URI"], x, parameterslist[0] = parameterslist[0].partition(" ")    #set URI parameter
        self.HTTPParams["HTTP_VERSION"], x, y = parameterslist[0].partition(" ")           #set the Version of HTTP, should be 1.1 since this is based upon rfc2616
        #parse the remaining header options
        for parameter in parameterslist[1:]:
            splitParameter = parameter.partition(':')
            self.HTTPParams[splitParameter[0]] = splitParameter[2][1:]
        
        #customized options, I want to see the client side port the server is sending information to
        #as well as the port the server is using to service the request
        self.HTTPParams["SOCK_NAME"] = self.channel.getsockname()
        self.HTTPParams["PEER_NAME"] = self.channel.getpeername()
        self.HTTPParams["REMOTE_ADDRESS"] = self.details

        if ("Content-Type" in self.HTTPParams) or ("Content-Length" in self.HTTPParams):
            for x in range(0, int(self.HTTPParams["Content-Length"])):
                self.content += self.channel.recv(1)
            print self.content

        self.uriList = self.HTTPParams['URI'].split("?")
        self.requestedFile = self.uriList[0]

        print self.HTTPParams

    def handleRequest(self):
        'This function processes the request and executes it'
        tempBody = ""
        tempHeader = ""

        extension = (os.path.splitext(self.requestedFile)[1])[1:]

        self.output = 'HTTP/1.1 200 OK\r\n'

        if (extension == "py"):
            'Section for handling CGI scripts'
            self.setCGI()
            CGIProcess = subprocess.Popen(self.requestedFile, bufsize=0, env=self.CGIEnv, stdout=subprocess.PIPE, stdin=subprocess.PIPE)
            tempHeader, tempBody = CGIProcess.communicate(self.content)[0].split("\r\n\r\n", 1)
        else:
            'Section for reading files. Set their mime type and stream the content'
            tempHeader += 'Content-Type: ' + MIMEList.get(extension, 'text/plain')
            streamFile = open(self.requestedFile, 'r')
            tempBody += streamFile.read() + '\n'
            streamFile.close()

        self.output += 'Date: ' + time.strftime("%a, %d %b %Y %H:%M:%S GMT", time.gmtime()) + '\r\n'
        self.output += 'Server: pyHTTPServer/0.1 (Linux)' + '\r\n'
        self.output += 'Content-Length: ' + str(len(tempBody)) + '\r\n'

        self.output += tempHeader + '\r\n\r\n' + tempBody

    def httpError(self, type):
        if type == 400:
            self.output = ( 'HTTP/1.1 400 Bad Request\r\nContent-Type: text/html\r\n\r\n'
                            '<html>\r\n<body>\r\n'
                            '   <p> 400 - Bad Request </p>\r\n'
                            '</body>\r\n</html>\r\n' )            
        elif type == 404:
            self.output = ( 'HTTP/1.1 404 Not Found\r\nContent-Type: text/html\r\n\r\n'
                            '<html>\r\n<body>\r\n'
                            '   <p> 404 - File Not Found </p>\r\n'
                            '</body>\r\n</html>\r\n' )

        self.channel.send ( self.output )
        self.channel.close()
        exit()

    def parseRequestedFile(self):
        if os.name == "nt":
            self.requestedFile = "NOT IMPLEMENTED"
        else:
            self.requestedFile = documentRoot[:-1] + self.requestedFile

        if os.path.isdir(self.requestedFile):
            self.requestedFile += "index.html"

        print ("Reading File: %s") % self.requestedFile

        if (not(in_directory(self.requestedFile, documentRoot))):
            self.httpError(400)

        if (not(os.path.isfile(self.requestedFile))):
            self.httpError(404)

    def setCGI(self):
        self.CGIEnv = {}
        TempCGIVar = {}
        TempCGIVar["AUTH_TYPE"] = None
        TempCGIVar["CONTENT_LENGTH"] = self.HTTPParams.get('Content-Length', None)
        TempCGIVar["CONTENT_TYPE"] = self.HTTPParams.get('Content-Type', None)
        TempCGIVar["DOCUMENT_ROOT"] = documentRoot
        TempCGIVar["GATEWAY_INTERFACE"] = "CGI/1.1"
        TempCGIVar["HTTP_ACCEPT"] = self.HTTPParams.get('Accept', None)
        TempCGIVar["HTTP_ACCEPT_CHARSET"] = self.HTTPParams.get('Accept-Charset', None)
        TempCGIVar["HTTP_ACCEPT_ENCODING"] = self.HTTPParams.get('Accept-Encoding', None)
        TempCGIVar["HTTP_ACCEPT_LANGUAGE"] = self.HTTPParams.get('Accept-Language', None)
        TempCGIVar["HTTP_CONNECTION"] = self.HTTPParams.get('Connection', "keep-alive")
        TempCGIVar["HTTP_COOKIE"] = self.HTTPParams.get('Cookie', None)
        TempCGIVar["HTTP_FROM"] = None
        TempCGIVar["HTTP_HOST"] = self.HTTPParams.get('Host', "localhost")
        TempCGIVar["HTTP_REFERER"] = self.HTTPParams.get('Referer', None)
        TempCGIVar["HTTP_USER_AGENT"] = self.HTTPParams.get('User-Agent', None)
        TempCGIVar["HTTPS"] = "off"
        TempCGIVar["LD_PRELOAD"] = ""
        TempCGIVar["PATH"] = os.environ['PATH']
        TempCGIVar["PATH_INFO"] = None
        TempCGIVar["PATH_TRANSLATED"] = None
        TempCGIVar["QUERY_STRING"] = self.uriList[1] if (len(self.uriList) > 1) else ""
        TempCGIVar["REMOTE_ADDR"] = self.details[0]
        TempCGIVar["REMOTE_HOST"] = None
        TempCGIVar["REMOTE_IDENT"] = None
        TempCGIVar["REMOTE_PORT"] = str(self.details[1])
        TempCGIVar["REMOTE_USER"] = None
        TempCGIVar["REQUEST_METHOD"] = self.HTTPParams.get('METHOD', "GET")
        TempCGIVar["REQUEST_URI"] = self.HTTPParams.get('URI', self.uriList[0])
        TempCGIVar["SCRIPT_FILENAME"] = self.requestedFile
        TempCGIVar["SCRIPT_NAME"] = self.uriList[0]
        TempCGIVar["SERVER_ADDR"] = "127.0.0.1"
        TempCGIVar["SERVER_ADMIN"] = "python@rocks.com"
        TempCGIVar["SERVER_NAME"] = "localhost"   
        TempCGIVar["SERVER_PORT"] = "80"
        TempCGIVar["SERVER_PROTOCOL"] = self.HTTPParams.get('httpVersion', "HTTP/1.1")
        TempCGIVar["SERVER_SIGNATURE"] = "<address>pyHTTPServer/0.1 (Linux) Server at localhost Port 80 </address>"
        TempCGIVar["SERVER_SOFTWARE"] = "pyHTTPServer/0.1 (Linux)"

        for envVariable in TempCGIVar:
            if (TempCGIVar[envVariable] != None):
                self.CGIEnv[envVariable] = TempCGIVar[envVariable] 
